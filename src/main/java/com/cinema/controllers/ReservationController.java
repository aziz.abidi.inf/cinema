package com.cinema.controllers;

import com.cinema.entities.Projection;
import com.cinema.entities.Reservation;
import com.cinema.entities.User;
import com.cinema.services.ProjectionService;
import com.cinema.services.ReservationService;
import com.cinema.services.UserInfo;
import com.cinema.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping("reservation")
public class ReservationController {

    private final ReservationService reservationService;
    private final ProjectionService projectionService;
    private final UserService userService;

    private final UserInfo userInfo;

    public ReservationController(ReservationService filmService, ProjectionService projectionService, UserService userService, UserInfo userInfo) {
        this.reservationService = filmService;
        this.projectionService = projectionService;
        this.userService = userService;
        this.userInfo = userInfo;
    }

    @GetMapping("/all")
    public String getAll(Model model){
        List<Reservation> reservationList = reservationService.getAll();
        model.addAttribute("reservationList", reservationList);
        return "reservation/listeReservation";
    }

    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") long id){
        Reservation reservation = new Reservation();
        Projection projection = projectionService.getProjectionById(id);
        String userName = userInfo.userName();
        User user = userService.getUserByUserName(userName);
        reservation.setUser(user);
        reservation.setProjection(projection);
        reservationService.addReservation(reservation);
        return "redirect:/reservation/mine";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        reservationService.DeleteReservation(id);
        String userName = userInfo.userName();
        User user = userService.getUserByUserName(userName);
        if(user.getRoles().contains("ADMIN")){
            return "redirect:/reservation/all";
        }
        return "redirect:/reservation/mine";
    }

    @GetMapping("/confirm/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        Reservation reservation = reservationService.getReservationById(id);
       reservation.setIsConfirmed(true);
       reservationService.addReservation(reservation);
        return "redirect:/reservation/all";
    }


    @GetMapping("/mine")
    public String mine(Model model){
        List<Reservation> reservationList = reservationService.getMine(userInfo.userName());
        model.addAttribute("reservationList", reservationList);
        return "reservation/mine";
    }


}
