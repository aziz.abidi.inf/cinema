package com.cinema.controllers;

import com.cinema.entities.Film;
import com.cinema.services.FilmService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping("film")
public class FilmController {

    private final FilmService filmService;

    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("/all")
    public String getAll(Model model){
        List<Film> filmList = filmService.getAll();
        model.addAttribute("filmList", filmList);
        return "film/listeFilm";
    }

    @GetMapping("/add")
    public String add(Model model){
        Film film = new Film();
        model.addAttribute("film", film);
        return "film/FilmForm";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("film") Film film){
        filmService.addFilm(film);
        return "redirect:/film/all";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        filmService.DeleteFilm(id);
        return "redirect:/film/all";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        Film film = filmService.getFilmById(id);
        model.addAttribute("film", film);
        return "film/updateFilm";
    }

    @PostMapping("/update")
    public String Update(@Validated Film film, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:film/edit/"+film.getId();
        }
        filmService.addFilm(film);
        return "redirect:/film/all";
    }
}
