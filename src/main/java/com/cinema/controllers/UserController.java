package com.cinema.controllers;

import com.cinema.dto.UpdateUserDTO;
import com.cinema.dto.UserDTO;
import com.cinema.entities.User;
import com.cinema.mapper.UserMapper;
import com.cinema.repositories.UserRepository;
import com.cinema.services.UserInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserInfo userInfo;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserController(UserInfo userInfo, UserRepository userRepository, UserMapper userMapper) {
        this.userInfo = userInfo;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }
    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDTO> userDTOS = userMapper.UsersToUserDTOS(users);
        return ResponseEntity.ok(userDTOS);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteUser(@PathVariable Long id) {
        userRepository.deleteById(id);
    }

    @PatchMapping("/{id}")
    public void updateUser(@RequestBody UpdateUserDTO updateUserDTO, @PathVariable int id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setUsername(updateUserDTO.getUsername());
            user.setBirthday(updateUserDTO.getBirthday());
            user.setAddress(updateUserDTO.getAddress());
            user.setName(updateUserDTO.getName());
            user.setEmail(updateUserDTO.getEmail());
            user.setPhoneNumber(updateUserDTO.getPhoneNumber());
            user.setFamilyName(user.getFamilyName());
            userRepository.save(user);
        }
    }

    public UserDetails getLoggedInUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return (UserDetails) authentication.getPrincipal();
        }
        return null;
    }
}
