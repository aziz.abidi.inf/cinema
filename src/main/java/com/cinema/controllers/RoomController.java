package com.cinema.controllers;

import com.cinema.entities.Room;
import com.cinema.services.RoomService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping("room")
public class RoomController {

    private final RoomService roomService;

    public RoomController(RoomService filmService) {
        this.roomService = filmService;
    }

    @GetMapping("/all")
    public String getAll(Model model){
        List<Room> roomList = roomService.getAll();
        model.addAttribute("roomList", roomList);
        return "room/listeRoom";
    }

    @GetMapping("/add")
    public String add(Model model){
        Room room = new Room();
        model.addAttribute("room", room);
        return "room/RoomForm";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("film") Room film){
        roomService.addRoom(film);
        return "redirect:/room/all";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        roomService.DeleteRoom(id);
        return "redirect:/room/all";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        Room room = roomService.getRoomById(id);
        model.addAttribute("room", room);
        return "room/updateRoom";
    }

    @PostMapping("/update")
    public String Update(@Validated Room room, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:film/edit/"+room.getId();
        }
        roomService.addRoom(room);
        return "redirect:/room/all";
    }
}
