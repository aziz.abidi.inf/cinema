package com.cinema.controllers;

import com.cinema.entities.Room;
import com.cinema.entities.Seat;
import com.cinema.services.RoomService;
import com.cinema.services.SeatService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping("seat")
public class SeatController {
    private final SeatService seatService;

    private final RoomService roomService;

    public SeatController(SeatService filmService, RoomService roomService) {
        this.seatService = filmService;
        this.roomService = roomService;
    }

    @GetMapping("/all")
    public String getAll(Model model){
        List<Seat> seatList = seatService.getAll();
        model.addAttribute("seatList", seatList);
        return "seat/listeSeat";
    }

    @GetMapping("/add")
    public String add(Model model){
        Seat seat = new Seat();
        List<Room> roomList = roomService.getAll();
        model.addAttribute("roomList", roomList);
        model.addAttribute("seat", seat);
        return "seat/SeatForm";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("film") Seat film){
        seatService.addSeat(film);
        return "redirect:/seat/all";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        seatService.DeleteSeat(id);
        return "redirect:/seat/all";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        Seat seat = seatService.getSeatById(id);
        List<Room> roomList = roomService.getAll();
        model.addAttribute("roomList", roomList);
        model.addAttribute("seat", seat);
        return "seat/updateSeat";
    }

    @PostMapping("/update")
    public String Update(@Validated Seat seat, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:film/edit/"+seat.getId();
        }
        seatService.addSeat(seat);
        return "redirect:/seat/all";
    }
}
