package com.cinema.controllers;

import com.cinema.entities.Film;
import com.cinema.entities.Projection;
import com.cinema.entities.Room;
import com.cinema.services.FilmService;
import com.cinema.services.ProjectionService;
import com.cinema.services.RoomService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/projection")
public class ProjectionController {

    private final ProjectionService projectionService;
    private final RoomService roomService;
    private final FilmService filmService;

    public ProjectionController(ProjectionService projectionService, RoomService roomService, FilmService filmService) {
        this.projectionService = projectionService;
        this.roomService = roomService;
        this.filmService = filmService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/all")
    public String getAll(Model model){
        List<Projection> projectionList = projectionService.getAll();
        model.addAttribute("projectionList", projectionList);
        return "projection/listeProjection";
    }

    @GetMapping("/list")
    public String getList(Model model){
        List<Projection> projectionList = projectionService.getAll();
        model.addAttribute("projectionList", projectionList);
        return "projection/allProjection";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/add")
    public String add(Model model){
        Projection projection = new Projection();
        List<Film> filmList = filmService.getAll();
        List<Room> roomList = roomService.getAll();
        model.addAttribute("roomList", roomList);
        model.addAttribute("filmList", filmList);
        model.addAttribute("projection", projection);
        return "projection/ProjectionForm";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/save")
    public String save(@ModelAttribute("projection") Projection projection){
        projectionService.addProjection(projection);
        return "redirect:/projection/all";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        projectionService.DeleteProjection(id);
        return "redirect:/projection/all";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        Projection projection = projectionService.getProjectionById(id);
        model.addAttribute("projection", projection);
        List<Film> filmList = filmService.getAll();
        List<Room> roomList = roomService.getAll();
        model.addAttribute("roomList", roomList);
        model.addAttribute("filmList", filmList);
        return "projection/updateProjection";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/update")
    public String Update(@Validated Projection projection, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:projection/edit/"+projection.getId();
        }
        projectionService.addProjection(projection);
        return "redirect:/projection/all";
    }


}
