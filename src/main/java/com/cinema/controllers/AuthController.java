package com.cinema.controllers;

import com.cinema.dto.CreateUserDTO;
import com.cinema.entities.User;
import com.cinema.mapper.UserMapper;
import com.cinema.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public AuthController(UserRepository ourUserRepo, PasswordEncoder passwordEncoder, UserMapper userMapper) {
        this.userRepository = ourUserRepo;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Object> saveUser(@RequestBody CreateUserDTO createUserDTO) {
        createUserDTO.setPassword(passwordEncoder.encode(createUserDTO.getPassword()));
        User user = userMapper.CreateUserDTOtoUser(createUserDTO);
        User result = userRepository.save(user);
        if (result.getId() > 0) {
            return ResponseEntity.ok("User Was Saved");
        }
        return ResponseEntity.status(404).body("Error, User Not Saved");
    }
}
