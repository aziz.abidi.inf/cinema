package com.cinema.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UserDTO {
    private int id;
    private String name;
    private String familyName;
    private Date birthday;
    private String email;
    private String username;
    private String roles;
    private int rating;
    private int phoneNumber;
    private String address;
}
