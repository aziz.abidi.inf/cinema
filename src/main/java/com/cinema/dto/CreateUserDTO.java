package com.cinema.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CreateUserDTO {
    private String name;
    private String familyName;
    private Date birthday;
    private String email;
    private String username;
    private String password;
    private String roles;
    private int phoneNumber;
    private String address;
}
