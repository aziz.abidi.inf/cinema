package com.cinema.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UpdateUserDTO {
    private String name;
    private String familyName;
    private Date birthday;
    private String email;
    private String username;
    private int phoneNumber;
    private String address;
}
