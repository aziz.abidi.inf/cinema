package com.cinema.mapper;

import com.cinema.dto.CreateUserDTO;
import com.cinema.dto.UserDTO;
import com.cinema.entities.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMapper {

    public User CreateUserDTOtoUser(CreateUserDTO createUserDTO){
        User user = new User();
        user.setUsername(createUserDTO.getUsername());
        user.setEmail(createUserDTO.getEmail());
        user.setAddress(createUserDTO.getAddress());
        user.setName(createUserDTO.getName());
        user.setBirthday(createUserDTO.getBirthday());
        user.setFamilyName(createUserDTO.getFamilyName());
        user.setRoles(createUserDTO.getRoles());
        user.setRating(0);
        user.setPhoneNumber(createUserDTO.getPhoneNumber());
        user.setPassword(createUserDTO.getPassword());
        return user;
    }
    public UserDTO UserToUserDTO(User user){
        return   UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .address(user.getAddress())
                .name(user.getName())
                .birthday(user.getBirthday())
                .familyName(user.getFamilyName())
                .roles(user.getRoles())
                .rating(user.getRating())
                .phoneNumber(user.getPhoneNumber())
                .username(user.getUsername())
                .build();
    }

    public List<UserDTO> UsersToUserDTOS(List<User> users){
        return users.stream().map(this::UserToUserDTO).toList();
    }
}
