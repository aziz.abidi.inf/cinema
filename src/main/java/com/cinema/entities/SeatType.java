package com.cinema.entities;

public enum SeatType {
    CLASSIC,
    COMFORT,
    DOUBLE
}
