package com.cinema.services;

import com.cinema.entities.Projection;
import com.cinema.entities.Room;
import com.cinema.repositories.RoomRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Room getRoomById(long id){
        return roomRepository.findById(id).get();
    }

    public List<Room> getAll(){
        return roomRepository.findAll();
    }

    public void DeleteRoom(long id){
        roomRepository.deleteById(id);
    }

    public void addRoom(Room room){
        roomRepository.save(room);
    }
}
