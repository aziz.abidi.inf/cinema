package com.cinema.services;

import com.cinema.entities.Seat;
import com.cinema.entities.User;
import com.cinema.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getSeatById(long id){
        return userRepository.findById(id).get();
    }

    public List<User> getAll(){
        return userRepository.findAll();
    }

    public void DeleteUser(long id){
        userRepository.deleteById(id);
    }

    public void addSeat(User seat){
        userRepository.save(seat);
    }

    public User getUserByUserName(String username){
        return userRepository.findByUsername(username).get();
    }
}
