package com.cinema.services;

import com.cinema.entities.Reservation;
import com.cinema.repositories.ReservationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;

    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public Reservation getReservationById(long id){
        return reservationRepository.findById(id).get();
    }

    public List<Reservation> getAll(){
        return reservationRepository.findAll();
    }

    public void DeleteReservation(long id){
        reservationRepository.deleteById(id);
    }

    public void addReservation(Reservation reservation){
        reservationRepository.save(reservation);
    }

    public List<Reservation> getMine(String userName){
        return  reservationRepository.findByUserUsername(userName);
    }

}
