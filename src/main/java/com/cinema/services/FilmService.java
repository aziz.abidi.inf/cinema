package com.cinema.services;

import com.cinema.entities.Film;
import com.cinema.repositories.FilmRespository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {
    private final FilmRespository filmRespository;

    public FilmService(FilmRespository filmRespository) {
        this.filmRespository = filmRespository;
    }

    public Film getFilmById(long id){
        return filmRespository.findById(id).get();
    }

    public List<Film> getAll(){
        return filmRespository.findAll();
    }

    public void DeleteFilm(long id){
        filmRespository.deleteById(id);
    }

    public void addFilm(Film film){
        filmRespository.save(film);
    }
}
