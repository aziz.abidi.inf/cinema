package com.cinema.services;

import com.cinema.entities.Projection;
import com.cinema.repositories.ProjectionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectionService {

    private final ProjectionRepository projectionRepository;

    public ProjectionService(ProjectionRepository projectionRepository) {
        this.projectionRepository = projectionRepository;
    }

    public Projection getProjectionById(long id){
        return projectionRepository.findById(id).get();
    }

    public List<Projection> getAll(){
        return projectionRepository.findAll();
    }

    public void DeleteProjection(long id){
        projectionRepository.deleteById(id);
    }

    public void addProjection(Projection projection){
        projectionRepository.save(projection);
    }
}
