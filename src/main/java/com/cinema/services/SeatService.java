package com.cinema.services;

import com.cinema.entities.Room;
import com.cinema.entities.Seat;
import com.cinema.repositories.RoomRepository;
import com.cinema.repositories.SeatRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeatService {

    private final SeatRepository seatRepository;

    public SeatService(SeatRepository seatRepository) {
        this.seatRepository = seatRepository;
    }


    public Seat getSeatById(long id){
        return seatRepository.findById(id).get();
    }

    public List<Seat> getAll(){
        return seatRepository.findAll();
    }

    public void DeleteSeat(long id){
        seatRepository.deleteById(id);
    }

    public void addSeat(Seat seat){
        seatRepository.save(seat);
    }
}
