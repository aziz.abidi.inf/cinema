package com.cinema.repositories;

import com.cinema.entities.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRespository extends JpaRepository<Film, Long> {
}
